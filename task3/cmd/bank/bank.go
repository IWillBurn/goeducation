package main

import (
	"bank/iternal/app/bank/api/handlers"
	"bank/iternal/app/bank/repository/accounts"
	"bank/iternal/app/bank/repository/bank"
	"bank/iternal/app/bank/repository/cards"
	"bank/iternal/app/bank/service"
	"bank/pkg"
	"context"
	"log/slog"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"
)

func main() {
	textHandler := slog.NewTextHandler(os.Stdout, nil)
	log := slog.New(textHandler)
	b := &bank.Bank{Accounts: make(map[int]accounts.Account), Cards: make(map[string]cards.Card)}

	accountId := service.CreateAccount(b, pkg.Person{Name: "Eugene", Surname: "Boiarnikov"}, 1111)
	b.Cards[accountId].Get(100000)

	service.CreateAccount(b, pkg.Person{Name: "Admin", Surname: "Admin"}, 2222)

	mux := http.NewServeMux()
	mux.HandleFunc("/deposit", handlers.ProductHandlers(b).Deposit)
	mux.HandleFunc("/pay", handlers.ProductHandlers(b).Pay)
	mux.HandleFunc("/balance", handlers.ProductHandlers(b).GetBalance)

	httpServer := http.Server{
		Addr:    ":8080",
		Handler: mux,
	}
	log.Info("SERVER STARTED")
	go func() {
		httpServer.ListenAndServe()
	}()

	ctx, stop := signal.NotifyContext(context.Background(), syscall.SIGINT, syscall.SIGTERM)
	defer stop()

	<-ctx.Done()
	shutdownCtx, cancel := context.WithTimeout(context.Background(), time.Second*50)
	defer cancel()

	go func() {
		httpServer.Shutdown(shutdownCtx)
		cancel()
	}()

	<-shutdownCtx.Done()
}
