package pkg

type AccountOperationResult struct {
	Value   int
	Result  bool
	Message string
}

type Person struct {
	Name    string
	Surname string
	Age     int
}

type Balance struct {
	Value int
}
