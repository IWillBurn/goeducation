package bank

import (
	"bank/iternal/app/bank/repository/accounts"
	"bank/iternal/app/bank/repository/cards"
)

type Bank struct {
	Accounts map[int]accounts.Account
	Cards    map[string]cards.Card
}
