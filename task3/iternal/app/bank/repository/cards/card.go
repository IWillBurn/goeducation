package cards

import (
	"bank/pkg"
)

type Card interface {
	Pay(value int, addresseeCard *Card) pkg.AccountOperationResult
	Get(value int) pkg.AccountOperationResult
	GetBalance() pkg.AccountOperationResult
	GetAccess(keyCode int) pkg.AccountOperationResult
}
