package cards

import (
	"bank/iternal/app/bank/repository/accounts"
	"bank/pkg"
)

type MasterCard struct {
	Account *accounts.Account
	Number  string
	KeyCode int
	Wastes  int
}

func (card *MasterCard) Waste(value int) pkg.AccountOperationResult {
	result := (*card.Account).Waste(value)
	return result
}

func (card *MasterCard) Pay(value int, addresseeCard *Card) pkg.AccountOperationResult {
	if addresseeCard == nil {
		return pkg.AccountOperationResult{
			Value:   0,
			Result:  false,
			Message: "Not Found",
		}
	}
	result := card.Waste(value)
	if result.Result {
		(*addresseeCard).Get(value)
	}
	return result
}

func (card *MasterCard) Get(value int) pkg.AccountOperationResult {
	return (*card.Account).Deposit(value)
}

func (card *MasterCard) GetBalance() pkg.AccountOperationResult {
	return (*card.Account).GetBalance()
}

func (card *MasterCard) GetAccess(keyCode int) pkg.AccountOperationResult {
	if keyCode == card.KeyCode {
		return pkg.AccountOperationResult{
			Value:   1,
			Result:  true,
			Message: "Authorized",
		}
	} else {
		return pkg.AccountOperationResult{
			Value:   0,
			Result:  false,
			Message: "Unauthorized",
		}
	}
}
