package accounts

import (
	"bank/pkg"
)

type Account interface {
	GetBalance() pkg.AccountOperationResult
	Deposit(value int) pkg.AccountOperationResult
	Waste(value int) pkg.AccountOperationResult
}
