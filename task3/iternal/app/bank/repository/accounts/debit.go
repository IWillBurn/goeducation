package accounts

import (
	"bank/pkg"
)

type DebitAccount struct {
	Id      int
	Person  pkg.Person
	Balance pkg.Balance
}

func (account *DebitAccount) GetBalance() pkg.AccountOperationResult {
	return pkg.AccountOperationResult{
		Value:   account.Balance.Value,
		Result:  true,
		Message: "OK",
	}
}

func (account *DebitAccount) Deposit(value int) pkg.AccountOperationResult {
	account.Balance.Value += value
	return pkg.AccountOperationResult{
		Value:   account.Balance.Value,
		Result:  true,
		Message: "OK",
	}
}

func (account *DebitAccount) Waste(value int) pkg.AccountOperationResult {
	if account.Balance.Value >= value {
		account.Balance.Value -= value
		return pkg.AccountOperationResult{
			Value:   account.Balance.Value,
			Result:  true,
			Message: "OK",
		}
	} else {
		return pkg.AccountOperationResult{
			Value:   account.Balance.Value,
			Result:  false,
			Message: "Not Enough Money",
		}
	}
}
