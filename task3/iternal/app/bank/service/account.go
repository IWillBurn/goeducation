package service

import (
	"bank/iternal/app/bank/repository/accounts"
	"bank/iternal/app/bank/repository/bank"
	"bank/iternal/app/bank/repository/cards"
	"bank/pkg"
	"strconv"
)

func CreateAccount(b *bank.Bank, person pkg.Person, keyCode int) string {
	accountId := AddDebitAccount(b, person)
	cardNumber := AddMasterCard(b, accountId, keyCode)
	return cardNumber
}

func AddDebitAccount(b *bank.Bank, person pkg.Person) int {
	accountId := len(b.Accounts)
	accountDebit := accounts.DebitAccount{
		Id:      accountId,
		Person:  person,
		Balance: pkg.Balance{Value: 0},
	}
	var account accounts.Account = &accountDebit
	b.Accounts[accountId] = account
	return accountId
}

func AddMasterCard(b *bank.Bank, accountId int, keyCode int) string {
	number := GetCardNumber(len(b.Cards))
	account := b.Accounts[accountId]
	accountLink := &account
	cardMaster := cards.MasterCard{
		Account: accountLink,
		Number:  number,
		KeyCode: keyCode,
		Wastes:  0,
	}
	var card cards.Card = &cardMaster
	b.Cards[number] = card
	return number
}

func GetCardNumber(number int) string {
	result := strconv.Itoa(number)
	for {
		if len(result) >= 16 {
			break
		}
		result = "0" + result
	}
	return result
}
