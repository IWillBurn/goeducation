package requests

type GetBalanceRequest struct {
	Number  string `json:"number"`
	KeyCode int    `json:"keyCode"`
}

type PayRequest struct {
	Number          string `json:"number"`
	KeyCode         int    `json:"keyCode"`
	NumberAddressee string `json:"numberAddressee"`
	Value           int    `json:"value"`
}

type DepositRequest struct {
	Number string `json:"number"`
	Value  int    `json:"value"`
}
