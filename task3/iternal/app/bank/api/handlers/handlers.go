package handlers

import (
	"bank/iternal/app/bank/api/requests"
	"bank/iternal/app/bank/api/responses"
	"bank/iternal/app/bank/repository/bank"
	"encoding/json"
	"fmt"
	"net/http"
)

type BankBalanceHandlers struct {
	Bank *bank.Bank
}

func ProductHandlers(bank *bank.Bank) *BankBalanceHandlers {
	return &BankBalanceHandlers{
		Bank: bank,
	}
}

func (h *BankBalanceHandlers) GetBalance(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		return
	}
	request := &requests.GetBalanceRequest{}
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(request)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	response := &responses.GetBalanceResponse{
		Balance: 0,
		Status:  false,
		Error:   "Rejected",
	}
	if h.Bank.Cards[request.Number] != nil && h.Bank.Cards[request.Number].GetAccess(request.KeyCode).Result {
		result := h.Bank.Cards[request.Number].GetBalance()
		response.Balance = result.Value
		response.Status = true
		response.Error = ""
	}
	responseJson, err := json.Marshal(response)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	fmt.Fprintf(w, string(responseJson))

	w.WriteHeader(http.StatusOK)
	return
}

func (h *BankBalanceHandlers) Pay(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		return
	}
	request := &requests.PayRequest{}
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(request)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	response := &responses.PayResponse{Status: false, Error: "Rejected"}
	if request.Number != request.NumberAddressee && h.Bank.Cards[request.Number] != nil && h.Bank.Cards[request.Number].GetAccess(request.KeyCode).Result {
		addressee := h.Bank.Cards[request.NumberAddressee]
		addresseeLink := &addressee
		if h.Bank.Cards[request.Number].Pay(request.Value, addresseeLink).Result {
			response.Status = true
			response.Error = ""
		}
	}

	responseJson, err := json.Marshal(response)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	fmt.Fprintf(w, string(responseJson))

	w.WriteHeader(http.StatusOK)
	return
}

func (h *BankBalanceHandlers) Deposit(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPatch {
		return
	}
	request := &requests.DepositRequest{}
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(request)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	response := &responses.DepositResponse{Balance: 0, Status: false, Error: "Rejected"}
	if h.Bank.Cards[request.Number] != nil {
		result := h.Bank.Cards[request.Number].Get(request.Value)
		response.Balance = result.Value
		response.Status = true
		response.Error = ""
	}
	responseJson, err := json.Marshal(response)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	fmt.Fprintf(w, string(responseJson))

	w.WriteHeader(http.StatusOK)
	return
}
