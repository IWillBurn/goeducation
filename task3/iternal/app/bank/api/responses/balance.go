package responses

type GetBalanceResponse struct {
	Balance int    `json:"balance"`
	Status  bool   `json:"status"`
	Error   string `json:"error"`
}

type PayResponse struct {
	Status bool   `json:"status"`
	Error  string `json:"error"`
}

type DepositResponse struct {
	Balance int    `json:"balance"`
	Status  bool   `json:"status"`
	Error   string `json:"error"`
}
