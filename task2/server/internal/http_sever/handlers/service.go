package handlers

import (
	"fmt"
	"net/http"
	"task2_server/app"
)

type AppServiceHandlers struct {
	a *app.App
}

func ServiceHandlers(application *app.App) *AppServiceHandlers {
	return &AppServiceHandlers{
		a: application,
	}
}

func (h *AppServiceHandlers) Version(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		return
	}
	fmt.Fprintf(w, "v1.0.0")
}
