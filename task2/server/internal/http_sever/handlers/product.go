package handlers

import (
	"encoding/base64"
	"encoding/json"
	"fmt"
	"math/rand"
	"net/http"
	"task2_server/app"
	"task2_server/models"
	"time"
)

type AppProductHandlers struct {
	a *app.App
}

func ProductHandlers(application *app.App) *AppProductHandlers {
	return &AppProductHandlers{
		a: application,
	}
}

func (h *AppProductHandlers) Decode(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		return
	}
	request := &models.DecodeRequest{}
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(request)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	text, err := base64.StdEncoding.DecodeString(request.S)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	response := &models.DecodeResponse{S: string(text)}
	responseJson, err := json.Marshal(response)

	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	fmt.Fprintf(w, string(responseJson))

	w.WriteHeader(http.StatusOK)
	return
}

func (h *AppProductHandlers) Hard(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		return
	}
	delayValue := rand.Intn(10) + 10
	delay := int64(delayValue) * time.Second.Nanoseconds()
	delayDuration := time.Duration(delay)
	time.Sleep(delayDuration)
	if rand.Intn(2) < 1 {
		w.WriteHeader(http.StatusOK)
	} else {
		w.WriteHeader(rand.Intn(27) + 500)
	}
	return
}
