package models

type DecodeRequest struct {
	S string `json:"inputString"`
}

type DecodeResponse struct {
	S string `json:"outputString"`
}
