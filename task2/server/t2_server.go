package main

import (
	"context"
	"net/http"
	"os/signal"
	"syscall"
	"task2_server/app"
	"task2_server/internal/http_sever/handlers"
	"time"
)

func main() {
	a := &app.App{}

	mux := http.NewServeMux()
	mux.HandleFunc("/version", handlers.ServiceHandlers(a).Version)
	mux.HandleFunc("/decode", handlers.ProductHandlers(a).Decode)
	mux.HandleFunc("/hard-op", handlers.ProductHandlers(a).Hard)

	httpServer := http.Server{
		Addr:    ":80",
		Handler: mux,
	}

	go func() {
		httpServer.ListenAndServe()
	}()

	ctx, stop := signal.NotifyContext(context.Background(), syscall.SIGINT, syscall.SIGTERM)
	defer stop()

	<-ctx.Done()
	shutdownCtx, cancel := context.WithTimeout(context.Background(), time.Second*50)
	defer cancel()

	go func() {
		httpServer.Shutdown(shutdownCtx)
		cancel()
	}()

	<-shutdownCtx.Done()
}
