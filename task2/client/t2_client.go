package main

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"task2_client/models"
	"time"
)

func main() {
	r, _ := http.Get("http://localhost/version")
	sb, _ := io.ReadAll(r.Body)
	fmt.Println(string(sb))

	request := models.DecodeRequest{S: "SGVsbG8sIFdvcmxk"}
	requestBytes, _ := json.Marshal(request)

	r, _ = http.Post("http://localhost/decode", "application/json", bytes.NewReader(requestBytes))

	response := &models.DecodeResponse{}
	decoder := json.NewDecoder(r.Body)
	_ = decoder.Decode(response)
	fmt.Println(response.S)

	ctx, cancel := context.WithTimeout(context.Background(), 15*time.Second)

	hardOpRequest, _ := http.NewRequestWithContext(ctx, "GET", "http://localhost/hard-op", nil)

	resp, _ := http.DefaultClient.Do(hardOpRequest)

	select {
	case <-ctx.Done():
		fmt.Println("false")
	default:
		fmt.Println("true, " + resp.Status[:3])
		cancel()
	}

}
