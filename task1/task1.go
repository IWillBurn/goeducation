package main

type Book struct {
	title        string
	author       string
	countOfPages int
}

func (book Book) GetTitle() string                 { return book.title }
func (book Book) SetTitle(title string)            { book.title = title }
func (book Book) GetAuthor() string                { return book.title }
func (book Book) SetAuthor(author string)          { book.author = author }
func (book Book) GetCountOfPages() int             { return book.countOfPages }
func (book Book) SetCountOfPages(countOfPages int) { book.countOfPages = countOfPages }

type Storage interface {
	Find(id int) Book
	Add(id int, book Book)
}

type MapStorage struct {
	storage map[int]Book
}

func (storage *MapStorage) Find(id int) Book {
	return storage.storage[id]
}
func (storage *MapStorage) Add(id int, book Book) {
	storage.storage[id] = book
}

type SliceStorageNode struct {
	id   int
	book Book
}
type SliceStorage struct {
	storage []SliceStorageNode
}

func (storage *SliceStorage) Find(id int) Book {
	for i := 0; i < len(storage.storage); i++ {
		if storage.storage[i].id == id {
			return storage.storage[i].book
		}
	}
	return Book{}
}
func (storage *SliceStorage) Add(id int, book Book) {
	storage.storage = append(storage.storage, SliceStorageNode{id, book})
}

type Librarian interface {
	GetId(name string) int
}

type LibrarianLinear struct {
	count int
	ids   map[string]int
}

func (librarian *LibrarianLinear) GetId(name string) int {
	var id int
	if librarian.ids[name] == 0 {
		id = librarian.count
		librarian.ids[name] = id
		librarian.count += 1
		return id
	}
	return librarian.ids[name]
}

type LibrarianHash struct {
	table      [1000009]bool
	multiplier int
}

func (librarian *LibrarianHash) GetId(name string) int {
	var result = 0
	var multiplier = 1
	for char := range name {
		result += int(name[char]) * multiplier
		result %= len(librarian.table)
		multiplier *= librarian.multiplier
		multiplier %= len(librarian.table)
	}
	librarian.table[result] = true
	return result
}

type Library struct {
	Storage
	Librarian
}

func (library *Library) AddBook(book Book) {
	var id = library.Librarian.GetId(book.GetTitle())
	library.Storage.Add(id, book)
}
func (library *Library) GetBookById(id int) Book {
	return library.Storage.Find(id)
}
func (library *Library) GetBookByName(name string) Book {
	return library.GetBookById(library.GetId(name))
}

func main() {

	var sliceStorage = &SliceStorage{storage: []SliceStorageNode{}}

	var mapStorage = &MapStorage{storage: map[int]Book{}}

	var librarianLinear = &LibrarianLinear{count: 1, ids: map[string]int{}}
	var librarianHash = &LibrarianHash{table: [1000009]bool{}, multiplier: 10241}

	var ISliceStorage Storage = sliceStorage
	var IMapStorage Storage = mapStorage
	var ILibrarianLinear Librarian = librarianLinear
	var ILibrarianHash Librarian = librarianHash

	var book1 = Book{title: "Book1", author: "Me1", countOfPages: 10}
	var book2 = Book{title: "Book2", author: "Me2", countOfPages: 11}
	var book3 = Book{title: "Book3", author: "Me3", countOfPages: 12}
	var book4 = Book{title: "Book4", author: "Me4", countOfPages: 13}
	var book5 = Book{title: "Book5", author: "Me5", countOfPages: 14}

	books := [5]Book{book1, book2, book3, book4, book5}

	println("first library: ")

	var library = Library{Storage: ISliceStorage, Librarian: ILibrarianLinear}
	library.AddBook(books[0])
	library.AddBook(books[1])
	library.AddBook(books[2])
	library.AddBook(books[3])
	library.AddBook(books[4])
	println(library.GetBookByName(books[0].GetTitle()).GetAuthor())
	println(library.GetBookByName(books[1].GetTitle()).GetAuthor())

	println("\n")
	println("second library: ")

	library = Library{Storage: IMapStorage, Librarian: ILibrarianHash}
	library.AddBook(books[0])
	library.AddBook(books[1])
	library.AddBook(books[2])
	library.AddBook(books[3])
	library.AddBook(books[4])
	println(library.GetBookByName(books[2].GetTitle()).GetAuthor())
	println(library.GetBookByName(books[3].GetTitle()).GetAuthor())
}
