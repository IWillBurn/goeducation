FROM prom/prometheus:v2.22.0

LABEL version="1.0.0"

COPY ./.config/prometheus.yml /etc/prometheus/prometheus.yml

EXPOSE 9090