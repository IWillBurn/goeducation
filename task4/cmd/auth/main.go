package main

import (
	"flag"
	"github.com/anonimpopov/hw4/internal/app"
	"github.com/anonimpopov/hw4/internal/logger"
)

func getConfigPath() string {
	var configPath string

	flag.StringVar(&configPath, "c", "../../.config/auth.yaml", "path to config file")
	flag.Parse()

	return configPath
}

func main() {
	config, err := app.NewConfig(getConfigPath())
	if err != nil {
		logger.ThrowLog("config error: ")
		logger.ThrowFatal(err)
	}

	a, err := app.New(config)
	if err != nil {
		logger.ThrowLog("can't create app")
		logger.ThrowFatal(err)
	}

	if err := a.Serve(); err != nil {
		logger.ThrowLog("maintenance error")
		logger.ThrowFatal(err)
	}
}
