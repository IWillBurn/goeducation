package logger

import (
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

var logger *zap.Logger
var loggerPrefix string = "logger: "
var loggerFatalPrefix string = "!!! FATAL: "

func initLogger() {
	config := zap.NewDevelopmentConfig()
	config.EncoderConfig.EncodeLevel = zapcore.CapitalColorLevelEncoder
	logger, _ = config.Build()
	zap.ReplaceGlobals(logger)
}

func init() {
	initLogger()
}

func ThrowLog(message string) {
	logger.Info(loggerPrefix+message, zap.String("message", loggerPrefix+message))
}

func ThrowFatal(error error) {
	logger.Info(loggerFatalPrefix)

	logger.Fatal(error.Error(), zap.Error(error))
}
